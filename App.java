package org.inguelberth.sistema;

import org.inguelberth.utilidades.Calculadora;

public class App{
	public static void main(String args[]){
		try{
			System.out.println("Resultado: "+Calculadora.operar(args[0]));
		}catch(Exception e){
			System.out.println("Error de sintaxis :(");
		}
	}
}
